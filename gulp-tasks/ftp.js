module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'emse.artbayard.ru',
				user: 'emse.artbayard.ru|ftp_emse',
				pass: '1234'
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}