module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.src.scss.src)
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.af(plugins.af_opts))
			.pipe(plugins.sass())
			.pipe(plugins.sourcemaps.write('.'))
			.pipe(gulp.dest(path.public.css))
			.pipe(plugins.connect.reload());
	}
}